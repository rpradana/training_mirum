module.exports = function(Cat) {

  Cat.greet = function(msg, cb) {
    cb(null, 'Greetings... ' + msg);
  }

  Cat.remoteMethod(
      'greet',
      {
        http: {path: '/greet', verb: 'post'},
        accepts: {arg: 'msg', type: 'string'},
        returns: {arg: 'greeting', type: 'string'}
      }
  );

};
