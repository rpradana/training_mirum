var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Kimin' });
});

router.get('/test', function(req, res, next) {
  res.render('test', { title: 'Express' });
});



module.exports = router;
