var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.put('/', function(req, res, next) {

  res.send('this is put request adding '+req.body.name);
});

router.post('/', function(req, res, next) {
  res.send('this is post request adding '+req.body.name);
});

router.delete('/', function(req, res, next) {
  res.send('this is post request deleting '+req.body.name);
});

router.get('/:name', function(req, res, next) {
  res.send('respond with a param name '+req.params.name);
});



module.exports = router;
